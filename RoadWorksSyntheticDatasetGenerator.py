import itertools
import json
import os
import sys
import random
import tempfile
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sumolib
import xmltodict
from scipy.interpolate import CubicSpline

import OSIRISToSUMOPolygon
from FCD import fcd_out_to_count_dataset
import csv


def create_sensors_coords(config, net: sumolib.net, coverage_perc: int) -> dict[str, dict[str:float]]:
    """
    Create sensors for the input road network

    :param net: the SUMO road network
    :param coverage_perc: the percentage of edges for which a sensor is created [0,100]
    :return: a dict where KEY is the ID of the lane, VALUE is a dict where 'x' and 'y' are the position of the sensor
    """
    all_edges_id = list(map(lambda e: e.getID(), net.getEdges()))
    edges_with_sensors = random.choices(all_edges_id, k=int(len(all_edges_id) * (coverage_perc / 100)))
    sensors_dict = {}

    if 'fcd.enable' in config and config['fcd.enable']:
        with open(config["fcd.observation_edges_file"]) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            rows = [row for row in csv_reader]
            rows = list(map(lambda s: s.replace("edge:", ""), list(itertools.chain(*rows))))
            [edges_with_sensors.append(e) for e in rows]
    for edge_id in edges_with_sensors:
        the_edge = net.getEdge(edge_id)
        lane_pos = random.uniform(0, the_edge.getLane(0).getLength())
        for lane in the_edge.getLanes():
            x, y = sumolib.geomhelper.positionAtShapeOffset(net.getLane(lane.getID()).getShape(), lane_pos)
            # TODO  SHOULD BE EDGE
            sensors_dict[lane.getID()] = {'x': x, 'y': y}  # {'lon': lon, 'lat': lat}
    return sensors_dict


def sensors_to_sumo_add_file(config,
                             net: sumolib.net,
                             sensor_coords_dict: dict[str, dict[str:float]],
                             out_fname: str,
                             induction_loop_out_fname="lane_output.xml"):
    """
    Create the SUMO XML sensors definition file

    :param net: the SUMO road network
    :param sensor_coords_dict: a dict where KEY is the ID of the lane, VALUE is a dict where 'x' and 'y' are the position of the sensor
    :param out_fname: the output XML file
    :param induction_loop_out_fname: the file where all sensors write the perceived data
    :return:
    """
    fd = open(out_fname, "w")
    sumolib.xml.writeHeader(fd, "$Id$", "additional")
    for lane_id, coordinates in sensor_coords_dict.items():
        x = coordinates['x']
        y = coordinates['y']
        lane_pos, _ = sumolib.geomhelper.polygonOffsetAndDistanceToPoint((x, y), net.getLane(lane_id).getShape())
        fd.write(
            f'    <inductionLoop id="%s" lane="%s" pos="%.2f" freq="%d" file="{induction_loop_out_fname}"/>\n' % (
                lane_id, lane_id, lane_pos, config['data_aggr_frequency']))
    fd.write("</additional>\n")
    fd.close()


def sensors_to_coords_file(net: sumolib.net, sensors_dict: dict, out_fname: str) -> None:
    coords_dict = []
    for lane_id in sensors_dict:
        lon, lat = net.convertXY2LonLat(sensors_dict[lane_id]['x'], sensors_dict[lane_id]['y'])
        coords_dict.append({'id': lane_id, 'lon': lon, 'lat': lat})
    coords_df = pd.DataFrame.from_records(coords_dict)
    coords_df.to_csv(out_fname, sep=';')


def generate_synthetic_traffic(sim_begin,
                               sim_end,
                               aggr_interval,
                               max_num_vehicles_per_aggr_interval=5000,
                               plot_data=False):
    ins_rates_fact = [0.1, 0.3, 0.67, 1, 0.7, 0.6, 0.7, 0.9, 0.4]  # traffic profile

    x = list(range(sim_begin, sim_end + 1, int((sim_begin + sim_end) / (len(ins_rates_fact) - 1))))
    # multiply the profile to the expected max number of vehicle (per time interval)
    ins_rates = np.array(ins_rates_fact) * max_num_vehicles_per_aggr_interval
    ins_rate_perturbation = [random.uniform(-500, 500) for _ in range(len(ins_rates))]  # hard-coded...ugly (wtf)
    ins_rates = np.array([np.max([0, ins_rates[i] + ins_rate_perturbation[i]]) for i in range(len(x))])
    cs = CubicSpline(x, ins_rates)
    xs = np.arange(sim_begin, sim_end + 1, aggr_interval)
    interp_data = cs(xs)
    if plot_data:
        fig, ax = plt.subplots(figsize=(6.5, 4))
        ax.plot(x, ins_rates, 'o', label='data')
        ax.plot(xs, cs(xs), label="S")
        ax.legend(loc='lower left', ncol=2)
        plt.show()
    return list(map(lambda td: int(td), interp_data))


def generate_random_pt(net, num_bus_lines, num_stops_per_line) -> None:
    """
    To rework...
    :param net:
    :param num_bus_lines:
    :param num_stops_per_line:
    :return:
    """
    radius = 200
    net_bbox = net.getBBoxXY()  # [(bottom_left_X, bottom_left_Y), (top_right_X, top_right_Y)]
    bus_lines = {}
    for i in range(num_bus_lines):
        rand_x = random.uniform(net_bbox[0][0], net_bbox[1][0])
        rand_y = random.uniform(net_bbox[0][1], net_bbox[1][1])
        neighbors_edges = net.getNeighboringEdges(rand_x, rand_y, radius)
        distances_and_edges = sorted([(dist, edge) for edge, dist in neighbors_edges], key=lambda x: x[0], reverse=True)
        dist, bus_stop_edge = distances_and_edges[0]

        bus_line = [bus_stop_edge]
        for stop_id in range(num_stops_per_line):
            last_stop_edge = bus_line[-1]
            x, y = sumolib.geomhelper.positionAtShapeOffset(last_stop_edge.getShape(), 0)
            neighbors_edges = net.getNeighboringEdges(x, y, radius)
            distances_and_edges = sorted([(dist, edge) for edge, dist in neighbors_edges], key=lambda x: x[0],
                                         reverse=True)
            dist, bus_stop_edge = distances_and_edges[0]
            bus_line.append(bus_stop_edge)

        bus_lines[i] = bus_line


def is_passenger_edge(edge: sumolib.net.edge) -> bool:
    """
    Return true if the input edge support the "passenger" vehicle type
    :param edge: a SUMO edge object
    :return:
    """
    for lane in edge.getLanes():
        if 'passenger' not in lane.getPermissions():
            return False
    return True


def generate_random_reroutes(net, net_path, output_path, perc) -> None:
    """
    Choose [perc]% edges from net that must be closed. Then, generate a rerouter additional file to be used in the simulation.
    The additional file contains the definition of the road closures and the reroutes that allows redirecting traffic during
    the simulation.
    :param net:
    :param perc_edges:
    :return:
    """

    edges = net.getEdges()
    # shuffle, then select the first n% of the edges so to obtain a random permutation of the edges
    random.shuffle(edges)
    edges_subset = edges[:int(len(edges) * (perc / 100))]

    edges_subset = list(filter(lambda e: is_passenger_edge(e), edges_subset))
    edges_id = list(map(lambda e: e.getID(), edges_subset))
    command = "python $SUMO_HOME/tools/generateRerouters.py -n {} -o {} -x \"{}\"".format(net_path,
                                                                                          output_path,
                                                                                          ','.join(edges_id))

    print('Executing: {}'.format(command))
    os.system(command)
    print('### DONE road closures generation')


def detectors_out_to_table(sim_data_df: pd.DataFrame, field_name: str) -> pd.DataFrame:
    """
    PLEASE NOTE: the lane ID and the sensors ID must match in order to avoid errors

    :param sim_data_df: the DATAFRAME obtained from the virtual detectors output.
    :param field_name: the name of the parameters to be used for the output data table
    :return:
    """
    # list_osm_edges = sim_data_df['interval_id'].unique()
    list_osm_edges = sim_data_df['@id'].unique()
    detectors_name = set(list_osm_edges)
    data_dict = {}
    for osm_id in detectors_name:
        data = sim_data_df.loc[sim_data_df['@id'] == osm_id][field_name]
        data_dict[osm_id] = data.to_numpy()
    data_df_formatted = pd.DataFrame.from_dict(data_dict)

    # data_df_formatted['ts_sumo'] = sim_data_df.interval_begin.unique()
    data_df_formatted['ts_sumo'] = sim_data_df['@begin'].unique()
    data_df_formatted = data_df_formatted.set_index('ts_sumo')
    return data_df_formatted


def generate_edgedata_add_file(sim_begin: int,
                               sim_end: int,
                               aggr_interval: int,
                               edgedata_add_xml_file: str,
                               edgedata_output_folder: str = None) -> None:
    """
    Generate SUMO edgedata additional file. This is required to generate edge-based output.

    See https://sumo.dlr.de/docs/Simulation/Output/Lane-_or_Edge-based_Traffic_Measures.html

    :param sim_begin: begin time of the simulation
    :param sim_end: end time of the simulation
    :param aggr_interval: aggregation interval
    :param edgedata_add_xml_file: the PATH to the XML edgedata additional file
    :param edgedata_output_folder: the FOLDER where the output edgedata files are written. The files in this folder are GENERATED BY THE SIMULATION
    :return:
    """
    with open(edgedata_add_xml_file, 'w') as outf:
        sumolib.writeXMLHeader(outf, "$Id$", "additional")
        a, b = itertools.tee(range(sim_begin, sim_end, aggr_interval))
        next(b, None)
        pairwise_ts = list(zip(a, b))

        for ts in pairwise_ts:
            if edgedata_output_folder is not None:
                the_str = f"    <edgeData id=\"{ts[0]}_to_{ts[1]}\" file=\"{edgedata_output_folder}/edgedata_{ts[0]}_to_{ts[1]}.out.xml\" begin=\"{ts[0]}\" end=\"{ts[1]}\" excludeEmpty=\"False\"/>\n"
            else:
                the_str = f"    <edgeData id=\"{ts[0]}_to_{ts[1]}\" file=\"edgedata_{ts[0]}_to_{ts[1]}.out.xml\" begin=\"{ts[0]}\" end=\"{ts[1]}\" excludeEmpty=\"False\"/>\n"
            outf.write(the_str)
        outf.write("</additional>\n")


def generate_osiris_rerouters(config: dict) -> None:
    """
    Generate the rerouters.

    :param config:
    :return:
    """
    geojson_path = config['with_roadworks']['osiris_geojson']
    rerouters_filename = config['with_roadworks']['rerouter_add_file']
    net_path = config['net_path']
    dic = json.load(open(geojson_path))

    roadworks_poly_xml_path = config['with_roadworks']['osiris_roadworks_poly']
    OSIRISToSUMOPolygon.create_roadwork_polygons(net_path, dic, roadworks_poly_xml_path)
    edges_in_roadworks_poly = OSIRISToSUMOPolygon.get_edges_per_taz(roadworks_poly_xml_path, net_path)
    OSIRISToSUMOPolygon.create_osiris_rerouters(list(edges_in_roadworks_poly.keys()), net_path, rerouters_filename)


def generate_traffic(config: dict) -> None:
    """
    Generate random traffic for the simulation model

    :param config: a dictionary containing the configuration of the dataset generator
    :return:
    """
    sim_begin = config['sim_begin_time']
    sim_end = config['sim_end_time']
    aggr_interval = config['data_aggr_frequency']
    max_num_vehicles_per_aggr_interval = config['max_num_vehicles_per_aggr_interval']
    out_folder = config['output_path']
    net_path = config['net_path']
    fringe_factor = config['fringe_factor']

    # 1-day sim
    ins_rates = generate_synthetic_traffic(sim_begin, sim_end, aggr_interval, max_num_vehicles_per_aggr_interval)
    ins_rates_str = ','.join(list(map(lambda id: str(id), ins_rates)))
    random_trip_command = (f"python $SUMO_HOME/tools/randomTrips.py -n {net_path} "
                           f"--insertion-rate={ins_rates_str} -e {str(sim_end)} "
                           f" -b {str(sim_begin)} "
                           f" -r {os.path.join(out_folder, 'routes.xml')} "
                           f" -o {os.path.join(out_folder, 'trips.xml')} "
                           f" --fringe-factor {str(fringe_factor)}")
    os.system(random_trip_command)


def generate_virtual_sensors(net: sumolib.net, config: dict) -> None:
    """
    Generate sensors for the input road network. The number of sensors is controlled by the property
    "perc_sensors_in_network" in the configuration file. This parameter indicates the percentage of edges in the road
    network that are associated with a virtual sensor.

    Sensors are placed lane-wise

    :param net:
    :param config: a dictionary containing the configuration of the dataset generator
    :return:
    """
    perc_sensors_in_network = config['perc_sensors_in_network']
    det_add_file_roadworks = config['with_roadworks']['detectors']['add_file']
    det_add_file_noroadworks = config['without_roadworks']['detectors']['add_file']
    det_output_file_roadworks = config['with_roadworks']['detectors']['detectors_output_file']
    det_output_file_noroadworks = config['without_roadworks']['detectors']['detectors_output_file']
    sensors_definition_fname = config['sensors_coordinates']

    # create [perc_sensors_in_network]% random sensors in the input road network
    sensors_dict = create_sensors_coords(config, net, perc_sensors_in_network)
    # create the additional data containing the coordinates of the sensors
    sensors_to_sumo_add_file(config, net, sensors_dict, det_add_file_noroadworks,
                             induction_loop_out_fname=det_output_file_noroadworks)

    sensors_to_sumo_add_file(config, net, sensors_dict, det_add_file_roadworks,
                             induction_loop_out_fname=det_output_file_roadworks)

    # save the ID/lon/lat information of the sensors to file (csv)
    sensors_to_coords_file(net, sensors_dict, sensors_definition_fname)


def generate_sumo_additional_files(config: dict) -> None:
    """
    Generate the SUMO edgedata-output xml additional files.

    See https://sumo.dlr.de/docs/Simulation/Output/Lane-_or_Edge-based_Traffic_Measures.html for more info about edgedata

    :param config: a dictionary containing the configuration of the dataset generator
    :return:
    """
    sim_begin = config['sim_begin_time']
    sim_end = config['sim_end_time']
    aggr_interval = config['data_aggr_frequency']
    edgedata_fname_wroadworks = config['with_roadworks']['edgedata']['add_file']
    edgedata_fname_noroadworks = config['without_roadworks']['edgedata']['add_file']
    edgedata_output_folder_name_wroadworks = config['with_roadworks']['edgedata']['edgedata_output_folder']
    edgedata_output_folder_name_noroadworks = config['without_roadworks']['edgedata']['edgedata_output_folder']

    generate_edgedata_add_file(sim_begin, sim_end, aggr_interval, edgedata_fname_noroadworks,
                               edgedata_output_folder_name_noroadworks)
    generate_edgedata_add_file(sim_begin, sim_end, aggr_interval, edgedata_fname_wroadworks,
                               edgedata_output_folder_name_wroadworks)

    os.makedirs(os.path.join(config['output_path'], edgedata_output_folder_name_noroadworks), exist_ok=True)
    os.makedirs(os.path.join(config['output_path'], edgedata_output_folder_name_wroadworks), exist_ok=True)


def run_simulation(config: dict) -> None:
    """
    Run SUMO

    :param config: a dictionary containing the configuration of the dataset generator
    :return:
    """
    sim_begin = config['sim_begin_time']
    sim_end = config['sim_end_time']
    out_folder = config['output_path']
    net_path = config['net_path']
    mesosim = config['mesosim']
    random_depart_offset = config['random_depart_offset']
    edgedata_fname_wroadworks = config['with_roadworks']['edgedata']['add_file']
    edgedata_fname_noroadworks = config['without_roadworks']['edgedata']['add_file']
    detectors_addfile_path = config['with_roadworks']['detectors']['add_file']
    detectors_addfile_noroadworks_path = config['without_roadworks']['detectors']['add_file']
    rerouter_addfile_path = config['with_roadworks']['rerouter_add_file']

    sumo_cmd = (f"sumo -n {net_path} -r {os.path.join(out_folder, 'routes.xml')} -e {str(sim_end)} "
                f" -b {str(sim_begin)} "
                f" -a {detectors_addfile_noroadworks_path},{edgedata_fname_noroadworks} "
                f" --device.rerouting.probability {str(1)}"
                f" --device.rerouting.period {str(20)}"
                f" --ignore-route-errors"
                f" --no-step-log"
                f" --no-warnings"
                f" --random"
                f" --random-depart-offset {str(random_depart_offset)}")
    if mesosim:
        sumo_cmd += " --mesosim"

    if 'fcd.enable' in config and config['fcd.enable']:
        prob = config['fcd.probability_vehicle_has_fcd'] / 100
        sumo_cmd += " --device.fcd.probability {} ".format(prob)
        sumo_cmd += " --fcd-output.filter-edges.input-file {} ".format(config['fcd.observation_edges_file'])
        sumo_cmd += " --fcd-output {} ".format(config['without_roadworks']['fcd.xml_output'])
    os.system(sumo_cmd)

    sumo_cmd = (f"sumo -n {net_path} -r {os.path.join(out_folder, 'routes.xml')} -e {str(sim_end)} "
                f" -b {str(sim_begin)} "
                # f" -a {detectors_addfile_path},{edgedata_fname_wroadworks},{rerouter_addfile_path} "
                f" --device.rerouting.probability {str(1)}"
                f" --device.rerouting.period {str(20)}"
                f" --ignore-route-errors"
                f" --no-step-log"
                f" --no-warnings"
                f" --random"
                f" --random-depart-offset {str(random_depart_offset)}")

    if 'execute_only_steps' in config and 0 in config['execute_only_steps']:
        sumo_cmd += f" -a {detectors_addfile_path},{edgedata_fname_wroadworks},{rerouter_addfile_path} "
    else:
        sumo_cmd += f" -a {detectors_addfile_path},{edgedata_fname_wroadworks} "

    if mesosim:
        sumo_cmd += " --mesosim"

    if 'fcd.enable' in config and config['fcd.enable']:
        prob = config['fcd.probability_vehicle_has_fcd'] / 100
        sumo_cmd += " --device.fcd.probability {} ".format(prob)
        sumo_cmd += " --fcd-output.filter-edges.input-file {} ".format(config['fcd.observation_edges_file'])
        sumo_cmd += " --fcd-output {} ".format(config['with_roadworks']['fcd.xml_output'])

    os.system(sumo_cmd)


def detectors_out_to_df(detectors_out_xml):
    with open(detectors_out_xml) as fd:
        doc = xmltodict.parse(fd.read())
        data = doc['detector']['interval']
        detectors_data_df = pd.DataFrame(data).fillna(0)
    return detectors_data_df


def process_simulation_output(net: sumolib.net, config: dict):
    detectors_out_xml_wroadworks = os.path.join(config['output_path'],
                                                config['with_roadworks']['detectors']['detectors_output_file'])
    detectors_out_xml_noroadworks = os.path.join(config['output_path'],
                                                 config['without_roadworks']['detectors']['detectors_output_file'])

    count_file_roadworks = config['with_roadworks']['count_dataset']
    count_file_noroadworks = config['without_roadworks']['count_dataset']
    speed_file_roadworks = config['with_roadworks']['speed_dataset']
    speed_file_noroadworks = config['without_roadworks']['speed_dataset']
    sim_data_df_no_roadworks = detectors_out_to_df(detectors_out_xml_noroadworks)
    sim_data_df_roadworks = detectors_out_to_df(detectors_out_xml_wroadworks)
    detectors_out_to_table(sim_data_df_no_roadworks, "@entered").to_csv(count_file_noroadworks, sep=';')
    detectors_out_to_table(sim_data_df_no_roadworks, "@speed").to_csv(speed_file_noroadworks, sep=';')
    detectors_out_to_table(sim_data_df_roadworks, "@entered").to_csv(count_file_roadworks, sep=';')
    detectors_out_to_table(sim_data_df_roadworks, "@speed").to_csv(speed_file_roadworks, sep=';')

    # process FCD
    if 'fcd.enable' in config and config['fcd.enable']:
        sim_begin = config['sim_begin_time']
        sim_end = config['sim_end_time']
        aggr_interval = config['data_aggr_frequency']
        # FCD traffic count (no roadworks)
        df = fcd_out_to_count_dataset(sim_begin, sim_end, aggr_interval, net,
                                      fcd_areas_poly_xml_file=config['fcd.observation_edges_file'],
                                      fcd_xml_out_fname=config['without_roadworks']['fcd.xml_output'],
                                      mesosim=config['mesosim'], )
        df.to_csv(config['without_roadworks']['fcd.count_dataset_output'], sep=";")

        # FCD speed (no roadworks)
        df = fcd_out_to_count_dataset(sim_begin, sim_end, aggr_interval, net,
                                      fcd_areas_poly_xml_file=config['fcd.observation_edges_file'],
                                      fcd_xml_out_fname=config['without_roadworks']['fcd.xml_output'],
                                      mesosim=config['mesosim'],
                                      mode=1)
        df.to_csv(config['without_roadworks']['fcd.speed_dataset_output'], sep=";")

        # FCD traffic count (roadworks)
        df = fcd_out_to_count_dataset(sim_begin, sim_end, aggr_interval, net,
                                      fcd_areas_poly_xml_file=config['fcd.observation_edges_file'],
                                      fcd_xml_out_fname=config['with_roadworks']['fcd.xml_output'],
                                      mesosim=config['mesosim'])
        df.to_csv(config['with_roadworks']['fcd.count_dataset_output'], sep=";")

        # FCD speed (roadworks)
        df = fcd_out_to_count_dataset(sim_begin, sim_end, aggr_interval, net,
                                      fcd_areas_poly_xml_file=config['fcd.observation_edges_file'],
                                      fcd_xml_out_fname=config['with_roadworks']['fcd.xml_output'],
                                      mesosim=config['mesosim'],
                                      mode=1)
        df.to_csv(config['with_roadworks']['fcd.speed_dataset_output'], sep=";")


def main_w_configuration(conf_file: str):
    config = json.load(open(conf_file))  # open("conf.json"))
    os.makedirs(config['output_path'], exist_ok=True)

    if config['net_path'].startswith('$'):
        config['net_path'] = os.environ[config['net_path'].strip('$')]

    net = sumolib.net.readNet(config['net_path'])
    # ------

    if ('execute_only_steps' in config and 0 in config['execute_only_steps']) or 'execute_only_steps' not in config:
        generate_osiris_rerouters(config)  # *** PHASE 0 ***
    if ('execute_only_steps' in config and 1 in config['execute_only_steps']) or 'execute_only_steps' not in config:
        generate_traffic(config)  # *** PHASE 1 ***
    if ('execute_only_steps' in config and 2 in config['execute_only_steps']) or 'execute_only_steps' not in config:
        generate_virtual_sensors(net, config)  # *** PHASE 2 ***
    if ('execute_only_steps' in config and 3 in config['execute_only_steps']) or 'execute_only_steps' not in config:
        generate_sumo_additional_files(config)  # *** PHASE 3***
    if ('execute_only_steps' in config and 4 in config['execute_only_steps']) or 'execute_only_steps' not in config:
        run_simulation(config)  # *** PHASE 4 ***
    if ('execute_only_steps' in config and 5 in config['execute_only_steps']) or 'execute_only_steps' not in config:
        process_simulation_output(net, config)  # *** PHASE 5 ***


if __name__ == '__main__':
    if len(sys.argv) == 1:
        main_w_configuration("conf.json")
    else:
        main_w_configuration(sys.argv[1])
