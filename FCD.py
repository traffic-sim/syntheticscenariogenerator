import itertools
import os
import random
import tempfile

import numpy as np
import pandas as pd
import sumolib


def generate_fcd_detection_areas(net_path, sensors_dict, out_poly_fname="fcd_areas.poly.add.xml"):
    net = sumolib.net.readNet(net_path)
    lanes_id = list(sensors_dict.keys())
    with open(out_poly_fname, 'w') as outf:
        sumolib.writeXMLHeader(outf, "$Id$", "additional")
        for lane_id in lanes_id:
            polygon = net.getLane(lane_id).getShape(includeJunctions=False)
            lonlat_points = list(map(lambda p: net.convertXY2LonLat(p[0], p[1]), polygon))
            # There is error for some polygon
            if len(polygon[0]) == 2:
                coords = ' '.join(list(map(lambda cc: "{},{}".format(cc[0], cc[1]), polygon)))
                string = "\t<poly id=\"{}\" shape=\"{}\"/>\n".format(lane_id, coords)
                outf.write(string)
        outf.write("</additional>\n")


def write_fcd_areas_edges_to_file(edges_with_sensors, perc_edges, out_fname="edge.ids.csv"):
    random.shuffle(edges_with_sensors)
    fcd_edges = edges_with_sensors[: int(len(edges_with_sensors) * (perc_edges / 100))]
    with open(out_fname, "w") as f:
        [f.write("edge:{}\n".format(edge_id)) for edge_id in fcd_edges]


def fcd_out_to_count_dataset(sim_begin: int,
                             sim_end: int,
                             aggr_period: int,
                             net: sumolib.net,
                             fcd_areas_poly_xml_file: str,
                             fcd_xml_out_fname: str,
                             mesosim: bool = False,
                             mode=0):
    """

    :param sim_begin:
    :param sim_end:
    :param aggr_period:
    :param net:
    :param fcd_areas_poly_xml_file:
    :param fcd_xml_out_fname:
    :param mesosim:
    :param mode: 0 == traffic count, 1 == speed
    :return:
    """
    edges_df = pd.read_csv(fcd_areas_poly_xml_file, sep=":", header=None, index_col=None, dtype={0: str, 1: str})
    fcd_areas_id = edges_df[1].to_list()
    all_lanes_id = []
    if not mesosim:
        for edge in fcd_areas_id:
            lanes = list(map(lambda e: e.getID(), net.getEdge(edge).getLanes()))
            [all_lanes_id.append(a) for a in lanes]
    else:
        all_lanes_id = fcd_areas_id
    output_table_cols_mapping = dict([(i, all_lanes_id[i]) for i in range(len(all_lanes_id))])

    edge_parameter = "vehicle_edge" if mesosim else "vehicle_lane"
    f = tempfile.NamedTemporaryFile(suffix='.csv')
    with open(f.name, 'w') as outf:
        xml2csv_command = "python $SUMO_HOME/tools/xml/xml2csv.py {} -o {}".format(fcd_xml_out_fname, outf.name)
        os.system(xml2csv_command)
        fcd_table = pd.read_csv(f.name, sep=";")
        fcd_table = fcd_table.dropna()

        fcd_table = fcd_table[fcd_table[edge_parameter].isin(all_lanes_id)]
        list_osm_edges = set(fcd_table[edge_parameter].unique())

        a, b = itertools.tee(range(sim_begin, sim_end + 1, aggr_period))
        next(b, None)
        pairwise_ts = list(zip(a, b))
        output_count_table = np.zeros((len(pairwise_ts), len(all_lanes_id)))

        for seg_idx, osm_id in enumerate(list_osm_edges):
            for ts_idx, ts in enumerate(pairwise_ts):
                # First, get the slice related to the current time interval
                # data = fcd_table.loc[fcd_table['timestep_time'].isin(range(ts[1]))]
                data = fcd_table.loc[fcd_table['timestep_time'].isin(range(ts[0], ts[1]))]
                if data.empty:
                    continue
                # Then, select those passing through osm_id
                if mode == 0:
                    df = data.loc[data[edge_parameter] == osm_id]['vehicle_id']
                    # data = data.loc[data[edge_parameter] == osm_id][edge_parameter]
                    # df = data.drop_duplicates('vehicle_id')
                    output_count_table[ts_idx][seg_idx] = df.unique().size
                else:
                    data = data.loc[data[edge_parameter] == osm_id]["vehicle_speed"]
                    output_count_table[ts_idx][seg_idx] = data.mean() if not data.empty else 0

        df_dataset = pd.DataFrame(output_count_table)
        df_dataset = df_dataset.rename(columns=output_table_cols_mapping)
        df_dataset['interval'] = list(map(lambda ts: ts[0], pairwise_ts))
        df_dataset = df_dataset.set_index('interval')
        return df_dataset


if __name__ == '__main__':
    net = sumolib.net.readNet(os.environ['BXL_NET'])

    # edges = list(map(lambda ed: ed.getID(), net.getEdges()))
    # write_fcd_areas_edges_to_file(edges, 1)

    df = fcd_out_to_count_dataset(0, 3600, 900, net,
                                  fcd_areas_poly_xml_file="/Users/dguastel/Desktop/test_fcd/edge.ids.csv",
                                  fcd_xml_out_fname="/Users/dguastel/Desktop/test_fcd/fcd.out.xml")
    df.to_csv("out.csv", sep=";")
    print('here')
