import os
import geopandas as gpd
import pandas as pd
import fiona  # don't remove please


def map_to_geojson(road_net_path, edgedata_out_xml, output_geojson_path):
    """
    Embed the edgedata output values into the network and export the network in a GEOJSON file

    :param road_net_path: the PATH to the SUMO road network XML
    :param edgedata_out_xml: the PATH to the edgedata output XML file
    :param output_geojson_path: the PATH to the output geojson file
    :return:
    """
    # step 1: net to geojson
    import tempfile
    tempf_geojson = tempfile.NamedTemporaryFile(suffix='.geojson')
    net2geojson_command = "python $SUMO_HOME/tools/net/net2geojson.py -n {} -o {}".format(road_net_path,
                                                                                          tempf_geojson.name)
    os.system(net2geojson_command)
    net_gdf = gpd.read_file(tempf_geojson)
    net_gdf['index'] = net_gdf['id']
    net_gdf = net_gdf.set_index('index')

    tempf_edgedata = tempfile.NamedTemporaryFile(suffix='.csv')
    xml2csv_command = "python $SUMO_HOME/tools/xml/xml2csv.py {} -o {}".format(edgedata_out_xml, tempf_edgedata.name)
    os.system(xml2csv_command)

    edgedata_df = pd.read_csv(tempf_edgedata.name, sep=";")
    edgedata_df = edgedata_df.loc[:,
                  ['edge_id',
                   'edge_occupancy',
                   'edge_density',
                   'edge_waitingTime',
                   'edge_speed',
                   'edge_speedRelative',
                   'edge_timeLoss',
                   'edge_traveltime',
                   'edge_sampledSeconds']]
    edgedata_df = edgedata_df.set_index('edge_id').fillna(0)

    df3 = net_gdf.join(edgedata_df).fillna(0)
    df3.to_file(output_geojson_path)


if __name__ == '__main__':
    # Example:
    map_to_geojson(os.environ['BXL_NET'],
                   "/Users/dguastel/Desktop/edgedata_46800_to_47700.out.xml",
                   "bxl_w_13h_edgedata_idExp0.geojson")
