import json
import os
import random
import sys
import tempfile
from functools import reduce

import numpy as np
import pandas as pd
import sumolib

import RoadWorksSyntheticDatasetGenerator
import TAZUtil


def sensors_mapping_to_file(net, sensors_dict, out_path_fname):
    mapping = {k: net.getLane(k).getID() for k, v in sensors_dict.items()}
    df = pd.DataFrame.from_dict(mapping, orient="index")
    df.index.name = "sensor_id"
    df.rename(columns={0: "lane_id"}, inplace=True)
    df.to_csv(out_path_fname, sep=";")


def create_sensors_coords(net: sumolib.net, taz_edge_dict) -> dict[str, dict[str:float]]:
    """
    Create sensors for the input road network

    :param net: the SUMO road network
    :return: a dict where KEY is the ID of the lane, VALUE is a dict where 'x' and 'y' are the position of the sensor
    """
    sensors_dict = {}
    for edges_list in list(taz_edge_dict.values()):
        for edge_id in edges_list:
            the_edge = net.getEdge(edge_id)
            lane_pos = random.uniform(0, the_edge.getLane(0).getLength())
            for lane in the_edge.getLanes():
                x, y = sumolib.geomhelper.positionAtShapeOffset(net.getLane(lane.getID()).getShape(), lane_pos)
                sensors_dict[lane.getID()] = {'x': x, 'y': y}
    return sensors_dict


def sensors_to_coords_file(net: sumolib.net, sensors_dict: dict, out_fname: str) -> None:
    coords_dict = []
    for lane_id in sensors_dict:
        lon, lat = net.convertXY2LonLat(sensors_dict[lane_id]['x'], sensors_dict[lane_id]['y'])
        coords_dict.append({'id': lane_id, 'lon': lon, 'lat': lat})
    coords_df = pd.DataFrame.from_records(coords_dict)
    coords_df.to_csv(out_fname, sep=';')


def num_sensors_in(taz_edge_dict):
    return reduce(lambda a, b: a + b, map(lambda x: len(x[1]), taz_edge_dict.items()))


def generate_virtual_sensors(net: sumolib.net, config: dict) -> None:
    """
    Generate sensors for the input road network. The number of sensors is controlled by the property
    "perc_sensors_in_network" in the configuration file. This parameter indicates the percentage of edges in the road
    network that are associated with a virtual sensor.

    Sensors are placed lane-wise

    :param net:
    :param config: a dictionary containing the configuration of the dataset generator
    :return:
    """
    edges_per_taz, _ = TAZUtil.get_edges_per_taz(config['taz.path'], config['net_path'])
    edges_in_scenario = int(len(net.getEdges()) * (config['perc_sensors_in_network'] / 100))
    train_sensors_edges = {k: v for k, v in edges_per_taz.items()}
    # remove until the right number of sensors
    if num_sensors_in(train_sensors_edges) > edges_in_scenario:
        while num_sensors_in(train_sensors_edges) > edges_in_scenario:
            k = random.choice(list(train_sensors_edges.keys()))
            if len(train_sensors_edges[k]) <= 2:
                continue
            train_sensors_edges[k].remove(random.choice(train_sensors_edges[k]))

    # now for each region, move some sensors to test set
    test_sensor_perc = config["taz.perc_test_sensors"] / 100
    test_sensors_edges = {}
    for taz_id in train_sensors_edges.keys():
        num_sensors_test = int(np.ceil(len(train_sensors_edges[taz_id]) * test_sensor_perc))
        random.shuffle(train_sensors_edges[taz_id])
        test_sensors_edges[taz_id] = train_sensors_edges[taz_id][:num_sensors_test]
        train_sensors_edges[taz_id] = train_sensors_edges[taz_id][num_sensors_test:]

    if not os.path.exists(os.path.join(config["output_path"], "train")):
        os.makedirs(os.path.join(config["output_path"], "train"), exist_ok=True)
    if not os.path.exists(os.path.join(config["output_path"], "test")):
        os.makedirs(os.path.join(config["output_path"], "test"), exist_ok=True)

    det_coords_train = os.path.join(config["output_path"], "train", "det_coordinates.csv")
    det_coords_test = os.path.join(config["output_path"], "test", "det_coordinates.csv")

    det_add_train = os.path.join(config["output_path"], "train", "det.add.xml")
    det_add_test = os.path.join(config["output_path"], "test", "det.add.xml")

    # create [perc_sensors_in_network]% random sensors in the input road network
    sensors_train = create_sensors_coords(net, train_sensors_edges)
    sensors_test = create_sensors_coords(net, test_sensors_edges)
    # create the additional data containing the coordinates of the sensors
    RoadWorksSyntheticDatasetGenerator.sensors_to_sumo_add_file(config, net, sensors_train, det_add_train,
                                                                induction_loop_out_fname="detector.out.xml")
    RoadWorksSyntheticDatasetGenerator.sensors_to_sumo_add_file(config, net, sensors_test, det_add_test,
                                                                induction_loop_out_fname="detector.out.xml")

    sensors_mapping_to_file(net, sensors_train,
                            os.path.join(config["output_path"], "train", "sensors_edges_mapping.csv"))
    sensors_mapping_to_file(net, sensors_test,
                            os.path.join(config["output_path"], "test", "sensors_edges_mapping.csv"))

    # save the ID/lon/lat information of the sensors to file (csv)
    sensors_to_coords_file(net, sensors_train, det_coords_train)
    sensors_to_coords_file(net, sensors_test, det_coords_test)


def generate_sumo_additional_files(config: dict) -> None:
    """
    Generate the SUMO edgedata-output xml additional files.

    See https://sumo.dlr.de/docs/Simulation/Output/Lane-_or_Edge-based_Traffic_Measures.html for more info about edgedata

    :param config: a dictionary containing the configuration of the dataset generator
    :return:
    """
    sim_begin = config['sim_begin_time']
    sim_end = config['sim_end_time']
    aggr_interval = config['data_aggr_frequency']
    RoadWorksSyntheticDatasetGenerator.generate_edgedata_add_file(sim_begin, sim_end, aggr_interval,
                                                                  os.path.join(config['output_path'], "train",
                                                                               "edgedata.add.xml"))
    RoadWorksSyntheticDatasetGenerator.generate_edgedata_add_file(sim_begin, sim_end, aggr_interval,
                                                                  os.path.join(config['output_path'], "test",
                                                                               "edgedata.add.xml"))

    os.makedirs(os.path.join(config['output_path'], "train", "edgedata_train"), exist_ok=True)
    os.makedirs(os.path.join(config['output_path'], "test", "edgedata_train"), exist_ok=True)


def run_simulation(config: dict) -> None:
    """
    Run SUMO

    :param config: a dictionary containing the configuration of the dataset generator
    :return:
    """
    sim_begin = config['sim_begin_time']
    sim_end = config['sim_end_time']
    out_folder = config['output_path']
    net_path = config['net_path']
    mesosim = config['mesosim']
    random_depart_offset = config['random_depart_offset']
    det_add_xml_train = os.path.join(config['output_path'], "train", "det.add.xml")
    det_add_xml_test = os.path.join(config['output_path'], "test", "det.add.xml")

    sumo_cmd = (f"sumo -n {net_path} -r {os.path.join(out_folder, 'routes.xml')} -e {str(sim_end)} "
                f" -b {str(sim_begin)} "
                f" -a {det_add_xml_test},{det_add_xml_train} "
                f" --device.rerouting.probability {str(1)}"
                f" --device.rerouting.period {str(20)}"
                f" --ignore-route-errors"
                f" --no-step-log"
                f" --no-warnings"
                f" --random"
                f" --random-depart-offset {str(random_depart_offset)}")
    if mesosim:
        sumo_cmd += " --mesosim"
    os.system(sumo_cmd)


def detectors_out_to_table(config, det_out_csv_path, field_name="interval_entered"):
    """
    PLEASE NOTE: the lane ID and the sensors ID must match in order to avoid errors
    :param det_out_csv_path:
    :param field_name:
    :return:
    """
    f = tempfile.NamedTemporaryFile(suffix='.csv')
    with open(f.name, 'w') as outf:
        os.system(f'python $SUMO_HOME/tools/xml/xml2csv.py {det_out_csv_path} -o {outf.name}')
        sim_data_df = pd.read_csv(outf.name, sep=";")

    list_osm_edges = sim_data_df['interval_id'].unique()
    data_dict = {}
    for osm_id in list_osm_edges:
        data = sim_data_df.loc[sim_data_df['interval_id'] == osm_id][field_name]
        data_dict[osm_id] = data.to_numpy()
    data_df_formatted = pd.DataFrame.from_dict(data_dict)

    sim_begin = config['sim_begin_time']
    sim_end = config['sim_end_time']
    aggr_interval = config['data_aggr_frequency']

    from datetime import datetime, timedelta
    ini_time_for_now = datetime.now()
    ts_midnight = datetime.combine(ini_time_for_now, datetime.min.time())
    all_ts = list(range(sim_begin, sim_end, aggr_interval))
    dates = list(map(lambda ts: (ts_midnight + timedelta(seconds=ts)).strftime("%Y/%m/%d, %H:%M:%S"), all_ts))

    data_df_formatted['ts'] = dates
    data_df_formatted.set_index('ts', inplace=True)
    return data_df_formatted


def process_simulation_output(config: dict):
    mesosim = config['mesosim']
    # NOTE: the traffic count is the value of 'interval_entered' from the edgedata output.
    traffic_count_field_name = 'interval_entered' if mesosim else 'interval_nVehEntered'
    det_add_xml_train = os.path.join(config['output_path'], "train", "detector.out.xml")
    det_add_xml_test = os.path.join(config['output_path'], "test", "detector.out.xml")
    count_dataset_train_path = os.path.join(config['output_path'], "train", "count_dataset.csv")
    count_dataset_test_path = os.path.join(config['output_path'], "test", "count_dataset.csv")
    detectors_out_to_table(config, det_add_xml_train, traffic_count_field_name).to_csv(count_dataset_train_path,
                                                                                       sep=";")
    detectors_out_to_table(config, det_add_xml_test, traffic_count_field_name).to_csv(count_dataset_test_path, sep=";")


def main_w_configuration(conf_file: str):
    config = json.load(open(conf_file))
    os.makedirs(config['output_path'], exist_ok=True)

    if config['net_path'].startswith('$'):
        config['net_path'] = os.environ[config['net_path'].strip('$')]

    net = sumolib.net.readNet(config['net_path'])
    # ------
    if ('execute_only_steps' in config and 1 in config['execute_only_steps']) or 'execute_only_steps' not in config:
        RoadWorksSyntheticDatasetGenerator.generate_traffic(config)  # *** PHASE 1 ***
    if ('execute_only_steps' in config and 2 in config['execute_only_steps']) or 'execute_only_steps' not in config:
        generate_virtual_sensors(net, config)  # *** PHASE 2 ***
    if ('execute_only_steps' in config and 3 in config['execute_only_steps']) or 'execute_only_steps' not in config:
        generate_sumo_additional_files(config)  # *** PHASE 3***
    if ('execute_only_steps' in config and 4 in config['execute_only_steps']) or 'execute_only_steps' not in config:
        run_simulation(config)  # *** PHASE 4 ***
    if ('execute_only_steps' in config and 5 in config['execute_only_steps']) or 'execute_only_steps' not in config:
        process_simulation_output(config)  # *** PHASE 5 ***


if __name__ == '__main__':
    if len(sys.argv) == 1:
        main_w_configuration("conf.json")
    else:
        main_w_configuration(sys.argv[1])
