import os
import sys
from xml.sax import parse

import sumolib
from shapely.geometry.polygon import Polygon

if 'SUMO_HOME' in os.environ:
    sys.path.append(os.path.join(os.environ['SUMO_HOME'], 'tools'))
    import edgesInDistricts
    from edgesInDistricts import DistrictEdgeComputer
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")


def get_edges_per_taz(taz_fname, net_path) -> (dict[str, str], dict[str, Polygon]):
    """
    Read the taz and the network files in input and return a mapping (dictionary) between TAZ and edges.
    :param taz_fname: PATH to taz file
    :param net_path:  PATH to road network file
    :return: a tuple: the first element is a dictionary where KEY is the ID of the TAZ, VALUE is a list of edges within the taz, the second a dictionary where the KEY is the taz ID, the value is a Polygon object
    """
    net = sumolib.net.readNet(net_path)
    reader = DistrictEdgeComputer(net)
    options = edgesInDistricts.parse_args(args=['-n', net_path])[0]
    poly_reader = sumolib.shapes.polygon.PolygonReader(True)
    parse(taz_fname, poly_reader)
    reader.computeWithin(poly_reader.getPolygons(), options)

    # key -> Edge object, VALUE -> Polygon object
    the_map = reader.getEdgeDistrictMap()
    the_tazs = list(set(the_map.values()))

    taz_polygon_dict = {}
    for taz in the_tazs:
        taz_polygon_dict[taz.id] = Polygon([net.convertXY2LonLat(p[0], p[1]) for p in taz.shape])

    edge_taz_dict = {}
    for edge, poly in the_map.items():
        if poly.id not in edge_taz_dict:
            edge_taz_dict[poly.id] = []
        edge_taz_dict[poly.id].append(edge.getID())
    return edge_taz_dict, taz_polygon_dict
