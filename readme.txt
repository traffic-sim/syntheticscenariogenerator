Install a virtual environment to install all the package you need there

--- Create the virtual environment

python -m venv venv

--- Activate the virtual environment (Linux)
source ./venv/bin/activate

--- Activate the virtual environment (Windows)
\venv\Scripts\activate

--- install all the package you need

pip install numpy pandas matplotlib scikit-learn sumolib

Important:

BXL_NET is an environment variable that points to the sumo network. In this folder, "bxl_subpart.net 1.xml".


--------------------------------------------------
				How does it work?
--------------------------------------------------
The script SyntheticDatasetGenerator.py generate two synthetic scenarios: one with all the edges open, and another one with some edges closed (matching somehow OSIRIS json files <<ws_in_progress.json>>). Once the scenarios 
are generated, the simulation is executed for each scenario. If the scripts are run several times, the same roads will be closed every time but the simulation results will be different because the traffic definition will change.

--------------------------------------------------
				Output folder
--------------------------------------------------

The file rerouters.add.xml contains the edges that were closed during the simulation (only one day is simulated). The tags closingReroute contain the id of the closed edges.

The file det_coordinates.csv contains the edge id where detectors are positioned and their latitud and longitud


