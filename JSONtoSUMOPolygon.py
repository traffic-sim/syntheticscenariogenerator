import json
import tempfile

import sumolib
import os
import sys
from xml.sax import parse

if 'SUMO_HOME' in os.environ:
    sys.path.append(os.path.join(os.environ['SUMO_HOME'], 'tools'))
    import edgesInDistricts
    from edgesInDistricts import DistrictEdgeComputer
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")


def createPolFile(net_path, dicFromMain, out_fname='out.xml'):
    dict_polygons = {}
    # retrieve polygons objects from json and add in dict_polygons
    for elem in dicFromMain['features']:
        coord = elem["geometry"]["coordinates"][0]
        id_ws = elem['properties']['ID_WS']
        is_perturbing_vehicles = elem['properties']['YN_VOIR_PERT']
        if is_perturbing_vehicles == 'Y':
            dict_polygons[id_ws] = coord

    f = None
    with tempfile.TemporaryDirectory() as td:
        f = os.path.join(td, 'temp.xml')
        
        with open(f, 'w') as outf:
            sumolib.writeXMLHeader(outf, "$Id$", "additional")
            
            for roadwork_id, polygon in dict_polygons.items():
                pol = [tuple(point) for point in polygon]
                # There is error for some polygon
                if len(pol[0]) == 2:
                    coords = ' '.join(list(map(lambda cc: "{},{}".format(cc[0], cc[1]), pol)))
                    string = "\t<poly id=\"{}\" shape=\"{}\"/>\n".format(roadwork_id, coords)
                    outf.write(string)
            outf.write("</additional>\n")
        os.system("polyconvert --xml {} -n {} -o {}".format(f, net_path, out_fname))


def get_edges_per_taz(taz_fname, net_path) -> (dict[str, str], dict):
    """
    Read the taz and the network files in input and return a mapping (dictionary) between TAZ and edges.
    :param taz_fname: PATH to taz file
    :param net_path:  PATH to road network file
    :return: a dictionary where KEY is the ID of the TAZ, VALUE is a list of edges within the taz
    """
    net = sumolib.net.readNet(net_path)
    reader = DistrictEdgeComputer(net)
    options = edgesInDistricts.parse_args(args=['-n', net_path])[0]
    poly_reader = sumolib.shapes.polygon.PolygonReader(False)
    parse(taz_fname, poly_reader)
    reader.computeWithin(poly_reader.getPolygons(), options)

    # key -> Edge object, VALUE -> Polygon object
    the_map = reader.getEdgeDistrictMap()

    output_dict = {}
    for edge, poly in the_map.items():
        output_dict[edge.getID()] = poly.id
    return output_dict


def support_passenger(edge):
    for lane in edge.getLanes():
        if 'passenger' not in lane.getPermissions():
            return False
    return True


def generate_rerouters(edges_id, net_path, output_path):
    net = sumolib.net.readNet(net_path)
    edges_subset = list(filter(lambda e: support_passenger(net.getEdge(e)), edges_id))

    command = f"python \"{os.environ['SUMO_HOME']}/tools/generateRerouters.py\" -n {net_path} -o {output_path} --closed-edges={','.join(edges_subset)}"

    print('Executing: {}'.format(command))
    os.system(command)
    print('### DONE closures generation')


if __name__ == '__main__':
    # TEST MAIN METHOD
    dic = json.load(open("ws_in_progress.json"))
    net_path = "bxl_subpart.net.xml" #os.environ['BXL_NET']
    createPolFile(net_path, dic, "roadworks.poly.xml")
    edges_in_roadworks_poly = get_edges_per_taz("roadworks.poly.xml", net_path)
    generate_rerouters(list(edges_in_roadworks_poly.keys()), net_path, 'rerouters_osiris.xml')
    print('OK')
