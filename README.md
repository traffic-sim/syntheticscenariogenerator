
# Traffic simulation method for synthetic data generation

This project presents a generic tool for generating synthetic traffic demand and how this can be used to assess the impact of disrupting events in simulated scenarios where we lack traffic data. Traffic simulation is performed using SUMO software.






## Installation

We suggest to install a virtual environment to install all the package you need to run the project there. `SUMO` python package can be installed via pip, together with the other required packages.

```bash
  $ pip install sumolib numpy pandas matplotlib
```

Additionally, `SUMO` software needs to be downloaded from its [Official website](https://eclipse.dev/sumo/) and installed following the instructions provided by the installation assistant. For Linux users, check the instruction provided [here](https://sumo.dlr.de/docs/Downloads.php#linux).

After the installation, check that the environment variable `SUMO_HOME` was created during the installation and it is pointing to the root folder of the SUMO source files. For instance, if SUMO was installed in the folder *Program Files (x86)*, `SUMO_HOME` should be pointed to `C:\Program Files (x86)\Eclipse\Sumo`



    
## How does it work?

The script SyntheticDatasetGenerator.py generate two synthetic scenarios: one with all the edges open, and another one with some edges closed (matching somehow OSIRIS json files <<ws_in_progress.json>>). Once the scenarios 
are generated, the simulation is executed for each scenario. If the scripts are run several times, the same roads will be closed every time but the simulation results will be different because the traffic definition will change.

## Output
## Acknowledgements



