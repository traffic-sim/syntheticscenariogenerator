import itertools
import os
import random
import json
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sumolib
from scipy.interpolate import CubicSpline

import OSIRISToSUMOPolygon


def create_sensors_coords(net: sumolib.net, coverage_perc: int) -> dict[str, dict[str:float]]:
    """
    Create sensors for the input road network

    :param net: the SUMO road network
    :param coverage_perc: the percentage of edges for which a sensor is created [0,100]
    :return: a dict where KEY is the ID of the lane, VALUE is a dict where 'x' and 'y' are the position of the sensor
    """
    all_edges_id = list(map(lambda e: e.getID(), net.getEdges()))
    edges_with_sensors = random.choices(all_edges_id, k=int(len(all_edges_id) * (coverage_perc / 100)))
    sensors_dict = {}
    for edge_id in edges_with_sensors:
        the_edge = net.getEdge(edge_id)
        lane_pos = random.uniform(0, the_edge.getLane(0).getLength())
        for lane in the_edge.getLanes():
            x, y = sumolib.geomhelper.positionAtShapeOffset(net.getLane(lane.getID()).getShape(), lane_pos)
            # lon, lat = net.convertXY2LonLat(x, y)
            sensors_dict[lane.getID()] = {'x': x, 'y': y}  # {'lon': lon, 'lat': lat}
    return sensors_dict


def sensors_to_sumo_add_file(net: sumolib.net,
                             sensor_coords_dict: dict[str, dict[str:float]],
                             out_fname: str,
                             induction_loop_out_fname="lane_output.xml"):
    """
    Create the SUMO XML sensors definition file

    :param net: the SUMO road network
    :param sensor_coords_dict: a dict where KEY is the ID of the lane, VALUE is a dict where 'x' and 'y' are the position of the sensor
    :param out_fname: the output XML file
    :param induction_loop_out_fname: the file where all sensors write the perceived data
    :return:
    """
    fd = open(out_fname, "w")
    sumolib.xml.writeHeader(fd, "$Id$", "additional")
    for lane_id, coordinates in sensor_coords_dict.items():
        x = coordinates['x']
        y = coordinates['y']
        lane_pos, _ = sumolib.geomhelper.polygonOffsetAndDistanceToPoint((x, y), net.getLane(lane_id).getShape())
        fd.write(
            f'    <inductionLoop id="%s" lane="%s" pos="%.2f" freq="900" file="{induction_loop_out_fname}"/>\n' % (
                lane_id, lane_id, lane_pos))
    fd.write("</additional>\n")
    fd.close()


def sensors_to_coords_file(net, sensors_dict, out_fname):
    coords_dict = []
    for lane_id in sensors_dict:
        x = sensors_dict[lane_id]['x']
        y = sensors_dict[lane_id]['y']
        lon, lat = net.convertXY2LonLat(x, y)
        coords_dict.append({'id': lane_id, 'lon': lon, 'lat': lat})
    coords_df = pd.DataFrame.from_records(coords_dict)
    coords_df.to_csv(out_fname, sep=';')


def generate_synthetic_traffic(sim_begin,
                               sim_end,
                               aggr_interval,
                               max_num_vehicles_per_aggr_interval=5000,
                               plot_data=False):
    ins_rates_fact = [0.1, 0.3, 0.67, 1, 0.7, 0.6, 0.7, 0.9, 0.4]
    x = list(range(sim_begin, sim_end + 1, int((sim_begin + sim_end) / (len(ins_rates_fact) - 1))))
    ins_rates = np.array(ins_rates_fact) * max_num_vehicles_per_aggr_interval
    ins_rate_perturbation = [random.uniform(-500, 500) for _ in range(len(ins_rates))]
    ins_rates = np.array([np.max([0, ins_rates[i] + ins_rate_perturbation[i]]) for i in range(len(x))])
    cs = CubicSpline(x, ins_rates)
    xs = np.arange(sim_begin, sim_end + 1, aggr_interval)
    interp_data = cs(xs)
    if plot_data:
        fig, ax = plt.subplots(figsize=(6.5, 4))
        ax.plot(x, ins_rates, 'o', label='data')
        ax.plot(xs, cs(xs), label="S")
        ax.legend(loc='lower left', ncol=2)
        plt.show()
    return list(map(lambda td: int(td), interp_data))


def generate_random_pt(net, num_bus_lines, num_stops_per_line):
    """
    To rework...
    :param net:
    :param num_bus_lines:
    :param num_stops_per_line:
    :return:
    """
    radius = 200
    net_bbox = net.getBBoxXY()  # [(bottom_left_X, bottom_left_Y), (top_right_X, top_right_Y)]
    bus_lines = {}
    for i in range(num_bus_lines):
        rand_x = random.uniform(net_bbox[0][0], net_bbox[1][0])
        rand_y = random.uniform(net_bbox[0][1], net_bbox[1][1])
        neighbors_edges = net.getNeighboringEdges(rand_x, rand_y, radius)
        distances_and_edges = sorted([(dist, edge) for edge, dist in neighbors_edges], key=lambda x: x[0], reverse=True)
        dist, bus_stop_edge = distances_and_edges[0]

        bus_line = [bus_stop_edge]
        for stop_id in range(num_stops_per_line):
            last_stop_edge = bus_line[-1]
            x, y = sumolib.geomhelper.positionAtShapeOffset(last_stop_edge.getShape(), 0)
            neighbors_edges = net.getNeighboringEdges(x, y, radius)
            distances_and_edges = sorted([(dist, edge) for edge, dist in neighbors_edges], key=lambda x: x[0],
                                         reverse=True)
            dist, bus_stop_edge = distances_and_edges[0]
            bus_line.append(bus_stop_edge)

        bus_lines[i] = bus_line
    print('here')


def support_passenger(edge):
    for lane in edge.getLanes():
        if 'passenger' not in lane.getPermissions():
            return False
    return True


def generate_random_reroutes(net, net_path, output_path, perc):
    """
    Choose [perc]% edges from net that must be closed. Then, generate a rerouter additional file to be used in the simulation.
    The additional file contains the definition of the road closures and the reroutes that allows redirecting traffic during
    the simulation.
    :param net:
    :param perc_edges:
    :return:
    """
    edges = net.getEdges()
    random.shuffle(edges)
    edges_subset = edges[:int(len(edges) * (perc / 100))]
    edges_subset = list(filter(lambda e: support_passenger(e), edges_subset))
    edges_id = list(map(lambda e: e.getID(), edges_subset))
    command = f"python \"{os.environ['SUMO_HOME']}/tools/generateRerouters.py\" -n {net_path} -o {output_path} -x \"{','.join(edges_id)}\""

    print('Executing: {}'.format(command))
    os.system(command)
    print('### DONE closures generation')


def detectors_out_to_table(sim_data_df, field_name):
    """
    PLEASE NOTE: the lane ID and the sensors ID must match in order to avoid errors
    :param real_data_df:
    :param sim_data_df:
    :param field_name:
    :return:
    """
    list_osm_edges = sim_data_df['interval_id'].unique()
    detectors_name = set(list_osm_edges)
    data_dict = {}
    for osm_id in detectors_name:
        data = sim_data_df.loc[sim_data_df['interval_id'] == osm_id][field_name]
        data_dict[osm_id] = data.to_numpy()
    data_df_formatted = pd.DataFrame.from_dict(data_dict)

    data_df_formatted['ts_sumo'] = sim_data_df.interval_begin.unique()
    data_df_formatted = data_df_formatted.set_index('ts_sumo')
    return data_df_formatted


def generate_edgedata_add_file(sim_begin, sim_end, aggr_interval, edgedata_add_xml_file, edgedata_output_folder):
    with open(edgedata_add_xml_file, 'w') as outf:
        sumolib.writeXMLHeader(outf, "$Id$", "additional")
        a, b = itertools.tee(range(sim_begin, sim_end, aggr_interval))
        next(b, None)
        pairwise_ts = list(zip(a, b))

        for ts in pairwise_ts:
            the_str = f"    <edgeData id=\"{ts[0]}_to_{ts[1]}\" file=\"{edgedata_output_folder}/edgedata_{ts[0]}_to_{ts[1]}.out.xml\" begin=\"{ts[0]}\" end=\"{ts[1]}\" excludeEmpty=\"False\"/>\n"
            outf.write(the_str)
        outf.write("</additional>\n")


if __name__ == '__main__':

    for seed in range(1997, 1998):
        print("Seed ", seed, "\n")
        
        out_folder = f"output_scenario_s{str(seed)}"
        os.makedirs(out_folder, exist_ok=True)
        
        net_path = "bxl_subpart.net1.xml" #os.environ['BXL_NET']
        sim_begin = 0
        sim_end = 86400
        aggr_interval = 900
        perc_sensors_in_network = 50
        fringe_factor = 10  # see https://sumo.dlr.de/docs/Tools/Trip.html#edge_probabilities
        random_depart_offset = 100  # see https://sumo.dlr.de/docs/Simulation/Randomness.html#departure_times
        mesosim = True

        net = sumolib.net.readNet(net_path)

        dic = json.load(open("ws_in_progress.json"))
        roadworks_poly_xml_path = os.path.join(out_folder, "roadworks.poly.xml")
        OSIRISToSUMOPolygon.createPolFile(net_path, dic, roadworks_poly_xml_path)
        edges_in_roadworks_poly = OSIRISToSUMOPolygon.get_edges_per_taz(roadworks_poly_xml_path, net_path)
        OSIRISToSUMOPolygon.generate_osiris_rerouters(list(edges_in_roadworks_poly.keys()),
                                                      net_path,
                                                      os.path.join(out_folder, "rerouters.add.xml"))

        ins_rates = generate_synthetic_traffic(sim_begin, sim_end, aggr_interval)
        ins_rates_str = ','.join(list(map(lambda id: str(id), ins_rates)))
        random_trip_command = (f"python \"{os.environ['SUMO_HOME']}/tools/randomTrips.py\" -n {net_path} "
                               f"--insertion-rate={ins_rates_str} -e {str(sim_end)} "
                               f"-b {str(sim_begin)} "
                               f"-r {os.path.join(out_folder, 'routes.xml')} "
                               f" -o {os.path.join(out_folder, 'trips.xml')} "
                               f"--random --seed {str(seed)}"
                               f" --fringe-factor {str(fringe_factor)}")

        os.system(random_trip_command)
        
        
        sensors_dict = create_sensors_coords(net, perc_sensors_in_network)
        sensors_to_sumo_add_file(net, sensors_dict, os.path.join(out_folder, "det.add.noroadworks.xml"),
                                 induction_loop_out_fname="lane_output_no_roadworks.xml")

        sensors_to_sumo_add_file(net, sensors_dict, os.path.join(out_folder, "det.add.wroadworks.xml"),
                                 induction_loop_out_fname="lane_output_w_roadworks.xml")

        generate_edgedata_add_file(sim_begin, sim_end, aggr_interval,
                                   os.path.join(out_folder, "edgedata.add.noroadworks.xml"),
                                   "edgedata_no_roadworks/")
        generate_edgedata_add_file(sim_begin, sim_end, aggr_interval,
                                   os.path.join(out_folder, "edgedata.add.wroadworks.xml"),
                                   "edgedata_w_roadworks/")

        os.makedirs(os.path.join(out_folder, "edgedata_no_roadworks/"), exist_ok=True)
        os.makedirs(os.path.join(out_folder, "edgedata_w_roadworks/"), exist_ok=True)
        
        sumo_cmd = (f"sumo -n {net_path} -r {os.path.join(out_folder, 'routes.xml')} -e {str(sim_end)} "
                    f" -b {str(sim_begin)} "
                    f" -a {os.path.join(out_folder, 'det.add.wroadworks.xml')},{os.path.join(out_folder, 'edgedata.add.wroadworks.xml')},{os.path.join(out_folder, 'rerouters.add.xml')} "
                    f" --device.rerouting.probability {str(1)}"
                    f" --device.rerouting.period {str(20)}"
                    f" --ignore-route-errors"
                    f" --no-step-log"
                    f" --no-warnings"
                    f" --random"
                    f" --random-depart-offset {str(random_depart_offset)}")
        
        if mesosim:
            sumo_cmd += " --mesosim"
        os.system(sumo_cmd)
        
        sumo_cmd = (f"sumo -n {net_path} -r {os.path.join(out_folder, 'routes.xml')} -e {str(sim_end)} "
                    f" -b {str(sim_begin)} "
                    f" -a {os.path.join(out_folder, 'det.add.noroadworks.xml')},{os.path.join(out_folder, 'edgedata.add.noroadworks.xml')} "
                    f" --device.rerouting.probability {str(1)}"
                    f" --device.rerouting.period {str(20)}"
                    f" --ignore-route-errors"
                    f" --no-step-log"
                    f" --no-warnings"
                    f" --random"
                    f" --random-depart-offset {str(random_depart_offset)}")
        if mesosim:
            sumo_cmd += " --mesosim"
        os.system(sumo_cmd)

        xml2csv_command = f"python \"{os.environ['SUMO_HOME']}/tools/xml/xml2csv.py\" {os.path.join(out_folder, 'lane_output_no_roadworks.xml')}"
        os.system(xml2csv_command)
        xml2csv_command = f"python \"{os.environ['SUMO_HOME']}/tools/xml/xml2csv.py\" {os.path.join(out_folder, 'lane_output_w_roadworks.xml')}"
        os.system(xml2csv_command)

        sim_data_df = pd.read_csv(os.path.join(out_folder, 'lane_output_no_roadworks.csv'), sep=";")
        dd = detectors_out_to_table(sim_data_df, 'interval_entered' if mesosim else 'interval_nVehEntered')
        dd.to_csv(os.path.join(out_folder, 'count_dataset_no_roadworks.csv'), sep=';')

        sim_data_df = pd.read_csv(os.path.join(out_folder, 'lane_output_w_roadworks.csv'), sep=";")
        dd = detectors_out_to_table(sim_data_df, 'interval_entered' if mesosim else 'interval_nVehEntered')
        dd.to_csv(os.path.join(out_folder, 'count_dataset_w_roadworks.csv'), sep=';')

        sensors_df = pd.DataFrame.from_dict(sensors_dict).T
        sensors_df.to_csv(os.path.join(out_folder, 'det_coordinates.csv'), sep=';')
